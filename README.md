# Installation

Simply run:
```
composer install
```

# Test run

Simply run the following in your terminal from the project root.
```
php -S localhost:8000 -t public/
```
