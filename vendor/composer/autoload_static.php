<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3191df975e0b31489448c797b2296545
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3191df975e0b31489448c797b2296545::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3191df975e0b31489448c797b2296545::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit3191df975e0b31489448c797b2296545::$classMap;

        }, null, ClassLoader::class);
    }
}
