<?php

namespace App;

class Request
{
    public function __get(string $prop): string
    {
        return $_REQUEST[$prop] ?? null;
    }

    public function send(string $url): mixed
    {
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_HEADER, 0);
        $data = curl_exec($req);
        curl_close($req);

        $data = $this->decode($data);

        return $data;
    }

    private function decode($data)
    {
        if($data !== false) {
            $data = json_decode($data, true);
        }

        return $data;
    }

}