<?php

namespace App;

class Response
{
    private array $errors;
    private array $signals;

    public function __construct()
    {
        $this->errors = [];
        $this->signals = [];
    }

    public function signal(string $signal)
    {
        $this->signals[] = $signal;
    }

    public function signals()
    {
        if(count($this->signals) > 0) {
            return $this->signals;
        }

        return false;
    }

    public function output()
    {
        if($this->errors()) print json_encode($this->errors());
        if($this->signals()) print json_encode($this->signals());

    }

    public function errors()
    {
        if(count($this->errors) > 0) {
            return $this->errors;
        }

        return false;
    }

    public function error(string $type)
    {
        $this->errors[] = $type;
    }
    
}