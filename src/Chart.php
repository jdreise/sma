<?php

namespace App;

class Chart
{
    private Request $request;
    private Response $response;
    private array $tickerData;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    private function ticker($exchange): void
    {
        $url = "http://cryptohopper-ticker-frontend.us-east-1.elasticbeanstalk.com/v1/$exchange/ticker";
        
        $data = $this->request->send($url);

        $this->tickerData = $data;
    }

    public function candle(string $exchange, string $market, int $startTimeStamp, int $endTimeStamp, string $period): void
    {
        if($this->validate("startTimeStamp", $startTimeStamp) && $this->validate("endTimeStamp", $endTimeStamp) && $this->validate("period", $period)) {
            $url = "http://cryptohopper-ticker-frontend.us-east-1.elasticbeanstalk.com/v1/$exchange/candles?pair=$market&start=$startTimeStamp&end=$endTimeStamp&period=$period";

            $data = $this->request->send($url);

            if($data !== false) {
                if(count($data) >= 55) {
                    $this->ticker($exchange);
                    $this->calculate($data);
                } else {
                    // Set response to not enough datapoints
                    $this->response->error("insufficient_datapoints");
                }
            }
        } else {
            $this->response->error("invalid_data_in_params");
        }
    }

    private function calculate($dataset): void
    {
        $sma8 = $this->sma(8, $dataset);
        $sma55 = $this->sma(55, $dataset);

        if($sma8 < $sma55) {
            $this->response->signal("sell");
        } else if($sma8 > $sma55) {
            $this->response->signal("purchase");
        } else {
            $this->response->signal("hodl");
        }

    }

    private function tickerValue()
    {
        $pairData = $this->tickerData["data"][$this->request->market];
        $lastValue = $pairData["last"];

        return $lastValue;
    }

    private function sma(int $amount, $dataset): float
    {
        $aggregateValues = 0;

        for($i = 0; $i < $amount; $i++) {
            $value = $dataset[$i]["Close"];

            if($i == ($amount - 1) && $this->tickerData !== false) $value = $this->tickerValue();

            $aggregateValues += $value;
        }

        $sma = $aggregateValues / $amount;

        return $sma;
    }

    public function validate(string $type, $data): bool
    {
        // Some very basic validation.
        switch($type) {
            case "exchange":
            case "market":
                // You could add every exchange and market/pair here, I won't.
                return true;
            case "startTimeStamp":
            case "endTimeStamp":
                return is_numeric($data);
            case "period":
                return in_array($data, ["1m", "5m", "15m", "30m", "1h", "2h", "4h", "1d"]);
            default:
                return false;
        }
    }

}