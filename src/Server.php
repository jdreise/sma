<?php

namespace App;

use App\Request;

class Server
{
    public function __construct()
    {
        $this->request = new Request;
        $this->response = new Response;
        $this->chart = new Chart($this->request, $this->response);
    }

    public function execute()
    {
        $this->chart->candle($this->request->exchange, $this->request->market, $this->request->startTimeStamp, $this->request->endTimeStamp, $this->request->period);

        $this->response->output();
    }
}